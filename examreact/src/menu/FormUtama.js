import React, { Component } from "react";
import NavbarNih from "./NavbarNih";
import { Link } from "react-router-dom";
import HasilFormU from "./HasilFormU";

export class FormUtama extends Component {
  constructor(props) {
    super(props);
    this.state = {
    //   data: [
        // {
          nik: "",
          nama: "",
          tempat: "",
          tanggal: "",
          jeniskelamin: "",
          alamat: "",
          agama: "",
          pekerjaan: "",
          kewarganegaraan: "",
          goldar: "",
        },
    //   ],
    // };

    handleChange = (e) => {
      this.setState({ [e.target.name]: e.target.value });
      console.log(e.target.value);
    };

    handleSubmit = (event) => {
      alert(
        "A name was submitted: " +
          this.state.nama +
          " " +
          this.state.tempat +
          " " +
          this.state.tanggal +
          " " +
          this.state.pengalaman
      );
      console.log(this.state.nama);
      event.preventDefault();
    };
  }

  render() {
    return (
      <div>
        <NavbarNih />

        <form onSubmit={this.handleSubmit} class="card">
          <h2 class="text1">Isi Data Diri</h2>
          <p>
            <label class="text">
              <b>NIK</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="nik"
              value={this.state.nik}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
          </p>
          <p>
            <label class="text">
              <b>Nama</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="nama"
              value={this.state.nama}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
          </p>
          <p>
            <label class="text">
              <b>Tempat Lahir</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="tempat"
              value={this.state.tempat}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
            <br />
            <label class="text">
              <b>Tanggal Lahir</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="tanggal"
              value={this.state.tanggal}
              onChange={this.handleChange}
              type="date"
              style={{ width: 150 }}
            />
          </p>
          <p>
            <label class="text">
              <b>Jenis Kelamin</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="jeniskelamin"
              value={this.state.jeniskelamin}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
          </p>
          <p>
            <label class="text">
              <b>Alamat</b>
            </label>
            <br />
            <textarea
              class="w3-input w3-border animasi"
              name="alamat"
              value={this.state.alamat}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
          </p>
          <p>
            <label class="text">
              <b>Agama</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="agama"
              value={this.state.agama}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
          </p>
          <p>
            <label class="text">
              <b>Status Pekerjaan</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="pekerjaan"
              value={this.state.pekerjaan}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
            <br />
          </p>
          <p>
            <label class="text">
              <b>Kewarganegaraan</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="kewarganegaraan"
              value={this.state.kewarganegaraan}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
            <br />
          </p>
          <p>
            <label class="text">
              <b>Golongan Darah</b>
            </label>
            <br />
            <input
              class="w3-input w3-border animasi"
              name="goldar"
              value={this.state.goldar}
              onChange={this.handleChange}
              type="text"
              style={{ width: 150 }}
            />
            <br />
          </p>
          {/* <table>
               <tbody>
                  {this.state.data.map((person, i) => <HasilFormU key = {i} 
                     data = {person} />)}
               </tbody>
            </table> */}
          <Link to="/HasilFormU">
            <p>
              <button class="btn" value="Submit">
                Submit
              </button>
            </p>
          </Link>
        </form>
      </div>
    );
  }

export default FormUtama;
