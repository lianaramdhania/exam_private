import React from 'react';
import './App.css';
import { FormUtama } from './menu/FormUtama'
import { Switch, Route } from 'react-router-dom';
import HasilFormU from './menu/HasilFormU';

function App() {
  return (
    <Switch>
      <Route path="/" exact component={FormUtama} />
      <Route path="/HasilFormU" component={HasilFormU} />
    </Switch>
  );
}

export default App;
