package com.nexsoft.exam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Worker extends Staff {
    public void listK() throws ParseException {
        int harga;
        int total;
        ArrayList<Integer> id = new ArrayList<>();
        id.add(1);
        id.add(2);
        id.add(3);
        id.add(4);

        ArrayList<String> nama = new ArrayList<>();
        nama.add("John");
        nama.add("Peter");
        nama.add("Linda");
        nama.add("Lucy");

        ArrayList<String> jabatan = new ArrayList<>();
        jabatan.add("Manager");
        jabatan.add("Staff");
        jabatan.add("Staff");
        jabatan.add("Manager");

        ArrayList<String> jenis = new ArrayList<>();
        jenis.add("Mobil");
        jenis.add("Motor");
        jenis.add("Motor");
        jenis.add("Mobil");

        ArrayList<String> waktuIn = new ArrayList<>();
        waktuIn.add("10:00");
        waktuIn.add("12:05");
        waktuIn.add("13:00");
        waktuIn.add("08:30");

        ArrayList<String> waktuOut = new ArrayList<>();
        waktuOut.add("13:10");
        waktuOut.add("14:00");
        waktuOut.add("13:50");
        waktuOut.add("16:30");

        System.out.println("ID  Nama    Jabatan     Kendaraan   IN      OUT     Lama    Harga   Sisa Tunjangan");

        for (int i = 0; i < id.size(); i++) {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            Date dateA1 = format.parse(waktuIn.get(i));
            Date dateA2 = format.parse(waktuOut.get(i));
            double difference = (dateA2.getTime() - dateA1.getTime());
            double detik = difference / 1000;
            double detiksejam = 3600;
            double jam = (Math.ceil(detik / detiksejam));

            if (jam <= 1 && jenis.get(i).equalsIgnoreCase("Mobil")) {
                harga = 5000;
                total = getTunjManajer() - harga;
                System.out.println(id.get(i) + "\t" + nama.get(i) + "\t" + jabatan.get(i) + "\t\t" + jenis.get(i) + "\t\t" + waktuIn.get(i) + "  " + waktuOut.get(i) + "\t" + jam + "J\t" + harga + "\t" + total);


            } else if (jam > 1 && jenis.get(i).equalsIgnoreCase("Mobil")) {
                int jamB = (int) (jam - 1);
                harga = 5000 + (jamB * 3000);
                total = getTunjManajer() - harga;
                System.out.println(id.get(i) + "\t" + nama.get(i) + "\t" + jabatan.get(i) + "\t\t" + jenis.get(i) + "\t\t" + waktuIn.get(i) + "  " + waktuOut.get(i) + "\t" + jam + "J\t" + harga + "\t" +total);


            } else if (jam <= 1 && jenis.get(i).equalsIgnoreCase("Motor")) {
                harga = 2000;
                total = getTunjStaff() - harga;
                System.out.println(id.get(i) + "\t" + nama.get(i) + "\t" + jabatan.get(i) + "\t\t" + jenis.get(i) + "\t\t" + waktuIn.get(i) + "  " + waktuOut.get(i) + "\t" + jam + "J\t" + harga + "\t" + total);


            } else if (jam > 1 && jenis.get(i).equalsIgnoreCase("Motor")) {
                int jamB = (int) (jam - 1);
                harga = 2000 + (jamB * 1000);
                total = getTunjStaff() - harga;
                System.out.println(id.get(i) + "\t" + nama.get(i) + "\t" + jabatan.get(i) + "\t\t" + jenis.get(i) + "\t\t" + waktuIn.get(i) + "  " + waktuOut.get(i) + "\t" + jam + "J\t" + harga + "\t" +total);

            }

        }

    }
}